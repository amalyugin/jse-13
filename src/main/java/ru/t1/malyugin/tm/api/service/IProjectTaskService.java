package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    Project removeProjectById(String projectId);

    Task unbindTaskFromProject(String projectId, String taskId);

}